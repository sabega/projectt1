/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectgui;

import blast.BlastController;
import java.io.File;
import java.io.PrintWriter;
import java.util.Observable;

/**
 *
 * @author Samuel Benavides García
 */
public class GlobalState extends Observable {

    float similarityPercentage;
    String querySequence, dataBaseFile, dataBaseIndexes, result = null;
    File outputFile;
    char seqType;
    boolean validSeq = false, validPer = false, validDb = false, validInd = false, validOut = false;
    boolean enabled;
    EastPanel east;
    WestPanel west;

    public GlobalState() {
        similarityPercentage = 0;
        querySequence = "";
        dataBaseFile = "";
        dataBaseIndexes = "";
        enabled = false;
    }

    public char getseqType() {
        return seqType;
    }

    public float getSimilarityPercentage() {
        return similarityPercentage;
    }

    public String getQuerySequence() {
        return querySequence;
    }

    public String getDataBaseFile() {
        return dataBaseFile;
    }

    public String getDataBaseIndex() {
        return dataBaseIndexes;
    }

    public String getResult() {
        return result;
    }

    public boolean getValidSeq() {
        return validSeq;
    }

    public boolean getValidPer() {
        return validPer;
    }

    public boolean getValidDb() {
        return validDb;
    }

    public boolean getValidInd() {
        return validInd;
    }

    public boolean getValidOut() {
        return validOut;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setSimilarityPercentage(float a) {
        similarityPercentage = a;
    }

    public void setQuerySequence(String a) {
        querySequence = a;
    }

    public void setDataBaseFile(String a) {
        dataBaseFile = a;
    }

    public void setDataBaseIndex(String a) {
        dataBaseIndexes = a;
    }

    public void setValidDb(boolean a) {
        validDb = a;
        verifyQuery(validDb, validInd, validPer, validSeq);
    }

    public void setValidInd(boolean a) {
        validInd = a;
        verifyQuery(validDb, validInd, validPer, validSeq);
    }

    public void setValidseq(boolean a) {
        validSeq = a;
        verifyQuery(validDb, validInd, validPer, validSeq);
    }

    public void setValidPer(boolean a) {
        validPer = a;
        verifyQuery(validDb, validInd, validPer, validSeq);
    }

    public void setValidOut(boolean a) {
        validOut = a;
    }

    private void setEnabled(boolean a) {
        enabled = a;
        if (enabled) {
            System.out.println("ENABLED!" + "  +  " + this.countObservers());
                notifyObservers();
        }
    }

    public void setSeqType(char a) {
        seqType = a;
    }

    public void setOutputFile(File a) {
        outputFile = a;
    }

    public void verifyQuery(boolean a, boolean b, boolean c, boolean d) {
        setEnabled(a && b && c && d);
    }

    public void doQuery() {
        BlastController bCnt = new BlastController();
        try {
            String results = bCnt.blastQuery(seqType, dataBaseFile,
                    dataBaseIndexes, similarityPercentage, querySequence);
            result = results;
            if (outputFile != null) {
                PrintWriter writer = new PrintWriter(outputFile.getAbsolutePath(), "UTF-8");
                writer.println(result);
                writer.close();
            }
            this.notifyObservers();
            result = null;
        } catch (Exception exc) {
            System.out.println("Error en la llamada: " + exc.toString());
        }
    }
}
