/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectgui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Samuel Benavides García
 */
public class WestPanel extends JPanel implements Observer {

    JLabel dbFile;
    JLabel indexFile;
    JLabel nOAs;
    JButton browsedb;
    JButton browseIndx;
    JButton query;
    JTextField dbFil;
    JTextField indFil;
    JRadioButton nucleotid;
    JRadioButton aminoacid;
    ButtonGroup group;
    JPanel westAux;
    JPanel radio;
    GlobalState gs;

    public WestPanel(GlobalState gs) {
        this.gs = gs;
        gs.addObserver(this);
        dbFile = new JLabel("Select database file");
        indexFile = new JLabel("Select database index file");
        nOAs = new JLabel("choose nucleotid or aminoacid");
        //JButton query = new JButton("Make query");
        browsedb = new JButton("Browse");
        browseIndx = new JButton("Browse");
        dbFil = new JTextField();
        indFil = new JTextField();
        nucleotid = new JRadioButton("Nucleotids");
        nucleotid.setSelected(true);
        aminoacid = new JRadioButton("Aminoacids");
        query = new JButton("Make query");
        query.setEnabled(false);

        //Group the radio buttons.
        group = new ButtonGroup();
        group.add(nucleotid);
        group.add(aminoacid);

        //create panels
        westAux = new JPanel();
        radio = new JPanel();

        //set panels parametres
        this.setLayout(new GridLayout(3, 1));
        radio.setLayout(new GridLayout(2, 1));
        westAux.setLayout(new GridLayout(4, 2));

        //create auxiliar labels
        JLabel aux1 = new JLabel("");
        JLabel aux2 = new JLabel("");
        aux1.setSize(browsedb.getSize());
        aux2.setSize(browsedb.getSize());

        //set panels content
        westAux.add(aux1);
        westAux.add(dbFile);
        westAux.add(browsedb);
        westAux.add(dbFil);
        westAux.add(aux2);
        westAux.add(indexFile);
        westAux.add(browseIndx);
        westAux.add(indFil);
        radio.add(nucleotid);
        radio.add(aminoacid);
        this.add(westAux);
        this.add(radio);
        this.add(query);

        indFil.getDocument().addDocumentListener(new IndexFileListener(indFil, gs));
        dbFil.getDocument().addDocumentListener(new TextFieldListener(dbFil, gs));
        query.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                gs.doQuery();
            }

        });
        browsedb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
                fc.addChoosableFileFilter(new FileNameExtensionFilter("data base files", "aa"));
                fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
                int returnVal = fc.showOpenDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    String file = fc.getSelectedFile().getAbsolutePath();
                    dbFil.setText(file);
                    gs.setDataBaseFile(file);
                    gs.setValidDb(true);
                }
            }
        });
        browseIndx.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
                fc.addChoosableFileFilter(new FileNameExtensionFilter("index files", "indexs"));
                fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
                int returnVal = fc.showOpenDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    String file = fc.getSelectedFile().getAbsolutePath();
                    indFil.setText(file);
                    gs.setDataBaseFile(file);
                    gs.setValidInd(true);
                }
            }
        });
    }
    public void setObserv(){
        gs.addObserver(this);
    }
    @Override
    public void update(Observable o, Object arg) {
        if (gs.getResult()==null)
            query.setEnabled(gs.getEnabled());            
    }

    class IndexFileListener extends TextFieldListener {

        public IndexFileListener(JTextField a, GlobalState gs) {
            super(a, gs);
        }

        @Override
        protected void check() {
            File test = new File(reference.getText());
            if (test.exists()) {
                reference.setForeground(Color.BLACK);
                gs.setValidInd(true);
                gs.setDataBaseIndex(reference.getText());
            } else {
                reference.setForeground(Color.RED);
                gs.setValidInd(false);
            }
        }
    }

    class DbFileListener extends TextFieldListener {

        public DbFileListener(JTextField a, GlobalState gs) {
            super(a, gs);
        }

        @Override
        protected void check() {
            File test = new File(reference.getText());
            if (test.exists()) {
                reference.setForeground(Color.BLACK);
                gs.setValidDb(true);
                gs.setDataBaseFile(reference.getText());
            } else {
                reference.setForeground(Color.RED);
                gs.setValidDb(false);
            }
        }
    }
}
