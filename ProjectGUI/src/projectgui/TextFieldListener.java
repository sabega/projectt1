package projectgui;

import java.awt.Color;
import java.io.File;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Samuel Benavides García
 */
public class TextFieldListener implements DocumentListener {

    JTextField reference;
    GlobalState gs;

    public TextFieldListener(JTextField a,GlobalState gs) {
        reference = a;
        this.gs=gs;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        check();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        check();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        check();
    }

    protected void check() {
        File test = new File(reference.getText());
        if (test.exists()) {
            reference.setForeground(Color.BLACK);
        } else {
            reference.setForeground(Color.RED);
        }
    }

 
}