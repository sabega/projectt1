/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectgui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Samuel Benavides García
 */
public class EastPanel extends JPanel implements Observer {

    JPanel eastAux;
    JPanel uppereast;
    JPanel lowereast;
    JLabel aux3;
    JLabel opFile;
    JLabel outputlab;
    JLabel percLab;
    JLabel seqLab;
    JButton browseOut;
    JTextField opFil;
    JTextField perc;
    JComboBox seq;
    JTextField outp;
    GlobalState gs;

    public EastPanel(GlobalState gs) {
        this.gs = gs;
        gs.addObserver(this);
        eastAux = new JPanel();
        uppereast = new JPanel();
        lowereast = new JPanel();
        setLayout(new GridLayout(3, 1));
        eastAux.setLayout(new GridLayout(2, 2));
        uppereast.setLayout(new GridLayout(4, 1));
        lowereast.setLayout(new GridLayout(2, 1));
        browseOut = new JButton("Browse");
        opFil = new JTextField();
        perc = new JTextField();
        seq = new JComboBox(new String[]{});
        outp = new JTextField();
        aux3 = new JLabel("");
        aux3.setSize(browseOut.getSize());
        opFile = new JLabel("Select output file");
        outputlab = new JLabel("Output");
        percLab = new JLabel("Type percentage");
        seqLab = new JLabel("Select or type a sequence to blast");
        eastAux.add(aux3);
        eastAux.add(opFile);
        eastAux.add(browseOut);
        eastAux.add(opFil);
        uppereast.add(seqLab);
        uppereast.add(seq);
        uppereast.add(percLab);
        uppereast.add(perc);
        lowereast.add(outputlab);
        lowereast.add(outp);
        seq.setEditable(true);
        add(uppereast);
        add(lowereast);
        add(eastAux);

        //add listeners
        opFil.getDocument().addDocumentListener(new OutputFileListener(opFil, gs));
        perc.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                check();
            }

            private void check() {
                try {
                    gs.setSimilarityPercentage(Float.parseFloat(perc.getText()) / 100);
                    perc.setForeground(Color.BLACK);
                    gs.setValidPer(true);
                } catch (NumberFormatException e) {
                    perc.setForeground(Color.RED);
                    gs.setValidPer(false);
                }
            }
        });
        ((JTextComponent) seq.getEditor().getEditorComponent()).getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                check();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                check();
            }

            public boolean isAlpha(String name) {
                char[] chars = name.toCharArray();
                for (char c : chars) {
                    if (!Character.isLetter(c)) {
                        return false;
                    }
                }
                return true;
            }

            private void check() {
                //System.out.println(seq.getEditor().getItem().toString());
                if (isAlpha(seq.getEditor().getItem().toString())) {
                    //System.out.println("letra");
                    gs.setQuerySequence(seq.getEditor().getItem().toString());
                    ((JTextField) seq.getEditor().getEditorComponent()).setForeground(Color.BLACK);
                    gs.setValidseq(true);
                } else {
                    ((JTextField) seq.getEditor().getEditorComponent()).setForeground(Color.RED);
                    gs.setQuerySequence("");
                    gs.setValidseq(false);
                }
            }
        });
        seq.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String x = String.valueOf(seq.getSelectedItem());
                gs.setQuerySequence(x);
            }
        });

        browseOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
                int returnVal = fc.showOpenDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    opFil.setText(fc.getSelectedFile().getAbsolutePath());
                }
            }
        });
    }
    public void setObserv(){
        gs.addObserver(this);
    }
    @Override
    public void update(Observable o, Object arg) {
        if (gs.getResult() != null) {
            outp.setText(gs.getResult());
        } else {
            seq.addItem(seq.getEditor().getItem().toString());
        }
    }

    class OutputFileListener extends TextFieldListener {

        public OutputFileListener(JTextField a, GlobalState gs) {
            super(a, gs);
        }

        @Override
        protected void check() {
            File test = new File(reference.getText());
            if (test.exists() & Files.isExecutable(test.toPath())) {
                reference.setForeground(Color.BLACK);
                gs.setValidDb(true);
            } else {
                test = new File(reference.getText().substring(0, reference.getText().lastIndexOf('\\') + 1));
                if (test.exists()) {
                    reference.setForeground(Color.BLACK);
                    gs.setValidDb(true);
                    test = new File(reference.getText());
                    try {
                        File file2 = new File(test.getAbsolutePath().substring(0, test.getAbsolutePath().length() - 1));
                        if (file2.exists())
                            file2.delete();
                        test.createNewFile();
                    } catch (IOException ex) {
                        outp.setText("problems creating file");
                    }
                    gs.setOutputFile(test);
                } else {
                    reference.setForeground(Color.RED);
                    gs.setValidDb(false);
                    gs.setOutputFile(null);
                }
            }
        }
    }
}
