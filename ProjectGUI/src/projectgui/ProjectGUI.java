/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectgui;

import java.awt.BorderLayout;
import javax.swing.JFrame;

/**
 *
 * @author Samuel Benavides García
 */
public class ProjectGUI {

    /**
     * @param args the command line arguments
     */
//   private static boolean queryType = true;//false=aminoacid true=nucleotids
    

    public static void main(String[] args) {
        //Declare components
        JFrame frame = new JFrame("Blast");
        GlobalState gs=new GlobalState();

        //set frame parameters
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);


        //adding contents to frame
        frame.getContentPane().add(new WestPanel(gs), BorderLayout.LINE_START);
        frame.getContentPane().add(new EastPanel(gs), BorderLayout.LINE_END);

        
        frame.pack();

    }
}

 